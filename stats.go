package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"sort"
	"strconv"
	"strings"
)

// Holds a Calendar object from the parsed JSON data.
type Calendar struct {
	Kind string
	// Etag    string
	Summary string
	// Updated string
	// TimeZone        string
	// AccessRole      string
	// DefaultReminders        string
	// NextPageToken   string
	Items []Item
}

// Holds items in a Calendar object. This is an array of calendar events
type Item struct {
	Kind string
	// Etag    string
	// Id      string
	// Status  string
	HtmlLink    string
	Created     string
	Updated     string
	Summary     string
	Description string
	// Location        string
	// Creator string
	// Organizer string
	Start StartType
	End   EndType
	// ICalUID string
	// Sequence int
	// Attendees string
	// GuestsCanInviteOthers string
	// PrivateCopy string
	// Reminders string
}

// Types for start and end date
type StartType struct{ DateTime string }

type EndType struct{ DateTime string }

// This will hold the Team list we pass to the template
type Cal struct {
	Calendars []string
}

// Use this to get month names in the x axis in graphs
var months = []string{"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	"SEP", "OCT", "NOV", "DEC"}

// 1. Determine the person from the request
// 2. Parse and load JSON data into memory
// 3. Return a pointer to it
func loadCalendar(w http.ResponseWriter, r *http.Request) *Calendar {
	name := r.URL.Path[len("/calendar/"):]

	data, _ := ioutil.ReadFile("data/" + name[:len(name)-4] + "@wso2.com.json")
	var c Calendar
	err := json.Unmarshal(data, &c)
	if err != nil {
		fmt.Println(err.Error())
	}

	return &c
}

// Get calendar events we're interested in
func getEvents(c *Calendar) map[string]map[string][]string {
	var person = make(map[string]map[string][]string)
	itemTotal := 0
	for _, v := range c.Items {
		// There are some empty events, skipping those. We need a start date
		if len(v.Start.DateTime) > 0 {
			if person[v.Start.DateTime[:4]] == nil {
				person[v.Start.DateTime[:4]] = make(map[string][]string)
			}
			person[v.Start.DateTime[:4]][v.Start.DateTime[5:7]] =
				append(person[v.Start.DateTime[:4]][v.Start.DateTime[5:7]], v.Summary)
			itemTotal += 1
		}
	}
	return person
}

func getSortedYears(events map[string]map[string][]string) []string {
	var sortedYears []string
	for i, _ := range events {
		if i == "2015" { // only 2014 stats for the moment
			sortedYears = append(sortedYears, i)
		}
	}
	sort.Strings(sortedYears)
	return sortedYears
}

func activityWiseData(w http.ResponseWriter, r *http.Request) {
	calendar := loadCalendar(w, r)
	events := getEvents(calendar)
	sortedYears := getSortedYears(events)

	for _, year := range sortedYears {
		var sortedMonths []string
		for i, _ := range events[year] {
			sortedMonths = append(sortedMonths, i)
		}
		sort.Strings(sortedMonths)

		fmt.Fprintf(w, "Month,Calls/Presentations,RFIs,RFPs,Demos,Other\n")

		for _, month := range sortedMonths {
			var rfis, rfps, calls, demos, other float64 // counting each event

			for _, title := range events[year][month] {
				t := strings.ToLower(title)
				switch true {
				case strings.Contains(t, "call") ||
					strings.Contains(t, "discussion") ||
					strings.Contains(t, "presentation"):
					calls += 1
				case strings.Contains(t, "demo"):
					demos += 1
				case strings.Contains(t, "rfi"):
					rfis += 1
				case strings.Contains(t, "rfp"):
					rfps += 1
				default:
					other += 1
				}

			}
			monthInt, _ := strconv.Atoi(month)
			fmt.Fprintf(w, "%s,%2.f,%2.f,%2.f,%2.f,%2.f\n",
				months[monthInt-1], calls, rfis, rfps, demos, other)
		}
	}
}

func productWiseData(w http.ResponseWriter, r *http.Request) {
	calendar := loadCalendar(w, r)
	events := getEvents(calendar)
	sortedYears := getSortedYears(events)

	for _, year := range sortedYears {
		var sortedMonths []string
		for i, _ := range events[year] {
			sortedMonths = append(sortedMonths, i)
		}
		sort.Strings(sortedMonths)

		fmt.Fprintf(w, "Products,API-M,AS,BAM,BPS,BRS,EMM,CEP,DSS,ELB,ESB,ES,G-Reg,IS,MB,SS,TS,UES,MSS,GW,DAS\n")

		for _, month := range sortedMonths {
			var apim, as, bam, bps, brs, emm, cep, dss, elb, esb, es, greg, is, mb, ss,
				ts, ues, mss, gw, das float64

			for _, title := range events[year][month] {
				t := strings.ToLower(title)

				if strings.Contains(t, "apim") ||
					strings.Contains(t, "api manager") ||
					strings.Contains(t, "api m") {
					apim += 1
				}
				if strings.Contains(t, "application server") ||
					strings.Contains(t, "applicationserver") ||
					strings.Contains(t, "appserver") ||
					strings.Contains(t, "app server") {
					as += 1
				}
				if strings.Contains(t, "bam") ||
					strings.Contains(t, "business activity monitor") {
					bam += 1
				}
				if strings.Contains(t, "bps") ||
					strings.Contains(t, "business process server") {
					bps += 1
				}
				if strings.Contains(t, "brs") ||
					strings.Contains(t, "business rules server") {
					brs += 1
				}
				if strings.Contains(t, "emm") ||
					strings.Contains(t, "enterprise mobility manager") {
					emm += 1
				}
				if strings.Contains(t, "cep") ||
					strings.Contains(t, "complex event processor") ||
					strings.Contains(t, "complex event") {
					cep += 1
				}
				if strings.Contains(t, "dss") ||
					strings.Contains(t, "data services") {
					dss += 1
				}
				if strings.Contains(t, "elb") ||
					strings.Contains(t, "elastic load") {
					elb += 1
				}
				if strings.Contains(t, "esb") ||
					strings.Contains(t, "enterprise service") {
					esb += 1
				}
				if strings.Contains(t, "enterprise store") ||
					strings.Contains(t, "enterprisestore") {
					es += 1
				}
				if strings.Contains(t, "greg") ||
					strings.Contains(t, "governance") ||
					strings.Contains(t, "g-reg") {
					greg += 1
				}
				if strings.Contains(t, "identity") {
					is += 1
				}
				if strings.Contains(t, "message broker") ||
					strings.Contains(t, "messagebroker") {
					mb += 1
				}
				if strings.Contains(t, "storageserver") ||
					strings.Contains(t, "storage server") {
					ss += 1
				}
				if strings.Contains(t, "taskserver") ||
					strings.Contains(t, "task server") {
					ts += 1
				}
				if strings.Contains(t, "user engagement server") ||
					strings.Contains(t, "userengagementserver") {
					ues += 1
				}
				if strings.Contains(t, "micro services server") ||
					strings.Contains(t, "microservices") ||
					strings.Contains(t, "mss") {
					mss += 1
				}
				if strings.Contains(t, "gateway") ||
					strings.Contains(t, "gw") {
					gw += 1
				}
				if strings.Contains(t, "data analytics server") ||
					strings.Contains(t, "das") {
					das += 1
				}
			}
			monthInt, _ := strconv.Atoi(month)
			fmt.Fprintf(w, "%s, %2.f, %2.f, %2.f, %2.f, %2.f, %2.f, %2.f, %2.f, %2.f, %2.f, %2.f, %2.f, %2.f, %2.f, %2.f, %2.f, %2.f, %2.f, %2.f, %2.f\n",
				months[monthInt-1], apim, as, bam, bps, brs, emm, cep, dss, elb,
				esb, es, greg, is, mb, ss, ts, ues, mss, gw, das)
		}
	}
}

// This list all files in data directory and create an array of people. Data directory hold all
// the calendars. Filename format is name@wso2.com.json
func getPeople() []string {
	var people []string
	files, _ := ioutil.ReadDir("data")
	for _, file := range files {
		people = append(people, file.Name()[:strings.Index(file.Name(), "@")])
		// fmt.Println(file.Name()[:strings.Index(file.Name(), "@")])
	}
	return people
}

func displayStats(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("index.html")

	var c Cal
	c.Calendars = getPeople()
	t.Execute(w, c)
}

func main() {
	http.HandleFunc("/products/", productWiseData)
	http.HandleFunc("/activity/", activityWiseData)
	http.HandleFunc("/", displayStats)

	fmt.Println("Listening at http://localhost:8080")
	http.ListenAndServe(":8080", nil)

}
