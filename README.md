# README #

This is a simple script to generate some stats out of pre sales activities. You wont be able to do much with this unless you have calendar data though.

### Download and save data ###

* Download JSON files, create a folder named 'data' and put the files
* Filename convention - email@domain.json
* Run it as go run stats.go
* Navigate to http://localhost:8080
